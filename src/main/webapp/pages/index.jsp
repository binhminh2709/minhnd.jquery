<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<style type="text/css">
	  body, td { font-size: 10pt; }
	  table { background-color: black; border: 1px black solid; border-collapse: collapse; }
	  th { border: 1px outset silver; background-color: maroon; color: white; }
	  tr { background-color: white; margin: 1px; }
	  tr.striped { background-color: blue; }
	  td { padding: 1px 8px; }
	</style>
<!--
Why need to load Ajax library from Google Code
1. Save bandwitdh – Google server is faster than your server , library load very fast from Google code.
2. Cache – High chance Google Ajax library is cached if user had visited some websites which deliver
from Google code.It will increase your website response time.

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js" type="text/javascript"></script>
 -->
<script src="<c:url value="/js/jquery/jquery.js" />"></script>


<script type="text/javascript">
  /**
   * 1. JavaScript
   * To call a function after page loaded, JavaScript uses the “window.onload“,
   * and “innerHTML” to create the content dynamically.
   */
  
  window.onload = function() {
    document.getElementById('msgid3').innerHTML = "This is Hello World by JavaScript";
  }

  /**
   * 2. jQuery
   * To call a function after page loaded, jQuery uses the “$(document).ready“,
   * and “html()” to create the content dynamically.
   */
  
  $(document).ready(function() {
    $("#msgid1").html("This is Hello World by JQuery 1<BR>");
  });
  
  $(function() {
    $("#msgid2").html("This is Hello World by JQuery 2<BR>");
  });
</script>
</head>
<body>
	<h1>Maven + Spring MVC Web Project Example</h1>

	<h3>Message : ${message}</h3>
	<h3>Counter : ${counter}</h3>

	This is Hello World by HTML
	<div id="msgid1"></div>

	<div id="msgid2"></div>

	<div id="msgid3"></div>

	<h1>A Table Zebra Stripes Effect With JQuery</h1>
	<script type="text/javascript">
    $(function() {
      //nth-child(even)”).addClass(“striped”) = every even row add “striped” CSS class dynamically.
      $("table tr:nth-child(even)").addClass("striped");
    });
  </script>


	<table>
		<tr>
			<th>ID</th>
			<th>Fruit</th>
			<th>Price</th>
		</tr>
		<tr>
			<td>1</td>
			<td>Apple</td>
			<td>0.60</td>
		</tr>
		<tr>
			<td>2</td>
			<td>Orange</td>
			<td>0.50</td>
		</tr>
		<tr>
			<td>3</td>
			<td>Banana</td>
			<td>0.10</td>
		</tr>
		<tr>
			<td>4</td>
			<td>strawberry</td>
			<td>0.05</td>
		</tr>
		<tr>
			<td>5</td>
			<td>carrot</td>
			<td>0.10</td>
		</tr>
	</table>

	<%
	  System.out.println("==========minhnd======");
	%>
</body>
</html>