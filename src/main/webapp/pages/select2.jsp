<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>select2</title>
<script type="text/javascript" src="../js/jquery/jquery-1.11.1.min.js" /></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
</head>
<body>
  <script type="text/javascript">
  //$('#select').select2();
  $("#select").select2({
    placeholder: "Select a state",
    allowClear: true
  });
  </script>

  <select id="select" name="select" class="js-example-basic-single">
    <option value="1">Aachener Stra&#223;e</option>
    <option value="2">Abbestra&#223;e</option>
    <option value="3">Adalbertstra&#223;e</option>
    <option value="4">Adam-von-Trott-Stra&#223;e</option>
    <option value="5">Adenauerplatz</option>
    <option value="6">Admiralstra&#223;e</option>
    <option value="7">Agathe-Lasch-Platz</option>
    <option value="8">Ahornallee</option>
    <option value="9">Ahrensfelder Chaussee</option>
    <option value="10">Ahrenshooper Stra&#223;e</option>
    <option value="11">Ahrweilerstra&#223;e</option>
    <option value="12">Akazienallee</option>
    <option value="13">Albrecht-Achilles-Stra&#223;e</option>
    <option value="14">Alemannenallee</option>
    <option value="15">Alexandrinenstra&#223;e</option>
    <option value="16">Alfred-D&#246;blin-Platz</option>
    <option value="17">Allendorfer Weg</option>
    <option value="18">Alte Allee</option>
    <option value="19">Alte Brauerei</option>
    <option value="20">Alte Jakobstra&#223;e</option>
    <option value="21">Altenburger Allee</option>
    <option value="22">Altenhofer Stra&#223;e</option>
    <option value="23">Alt-Lietzow</option>
    <option value="24">Alt-Stralau</option>
    <option value="25">Am Bahnhof Grunewald</option>
    <option value="26">Am Bahnhof Jungfernheide</option>
    <option value="27">Am Bahnhof Westend</option>
    <option value="28">Am Berl</option>
    <option value="29">Am Breiten Luch</option>
    <option value="30">Am Comeniusplatz</option>
    <option value="31">Am Containerbahnhof</option>
    <option value="32">Am D&#246;rferweg</option>
    <option value="33">Am Dornbusch</option>
    <option value="34">Am Faulen See</option>
    <option value="35">Am Fliederbusch</option>
    <option value="36">Am Glockenturm</option>
    <option value="37">Am G&#252;terbahnhof Halensee</option>
    <option value="38">Am Gutshof</option>
    <option value="39">Am Hain</option>
    <option value="40">Am Heidebusch</option>
    <option value="41">Am Johannistisch</option>
    <option value="42">Am Oberbaum</option>
    <option value="43">Am Ostbahnhof</option>
    <option value="44">Am Postbahnhof</option>
    <option value="45">Am Postfenn</option>
    <option value="46">Am Postfenn</option>
    <option value="47">Am Rudolfplatz</option>
    <option value="48">Am Rupenhorn</option>
    <option value="49">Am Schillertheater</option>
    <option value="50">Am Speicher</option>
    <option value="51">Am Spreebord</option>
    <option value="52">Am Tempelhofer Berg</option>
    <option value="53">Am Vogelherd</option>
    <option value="54">Am Volkspark</option>
    <option value="55">Am Weinhang</option>
    <option value="56">Am Westkreuz</option>
    <option value="57">Am Wriezener Bahnhof</option>
    <option value="58">Amselstra&#223;e</option>
    <option value="59">Amtsgerichtsplatz</option>
    <option value="60">An der Barthschen Promenade</option>
    <option value="61">An der Brauerei</option>
    <option value="62">An der Flie&#223;wiese</option>
    <option value="63">An der Margaretenh&#246;he</option>
    <option value="64">An der Michaelbr&#252;cke</option>
    <option value="65">An der Ostbahn</option>
    <option value="66">An der Schillingbr&#252;cke</option>
    <option value="67">Andreasplatz</option>
    <option value="68">Andreasstra&#223;e</option>
    <option value="69">Angerburger Allee</option>
    <option value="70">Anhalter Stra&#223;e</option>
    <option value="71">Anna-Ebermann-Stra&#223;e</option>
    <option value="72">Annemariestra&#223;e</option>
    <option value="73">Arcostra&#223;e</option>
    <option value="74">Arendsweg</option>
    <option value="75">Arndtstra&#223;e</option>
    <option value="76">Arnimstra&#223;e</option>
    <option value="77">Arysallee</option>
    <option value="78">Aschaffenburger Stra&#223;e</option>
    <option value="79">Askanischer Platz</option>
    <option value="80">A&#223;mannshauser Stra&#223;e</option>
    <option value="81">Astridstra&#223;e</option>
    <option value="82">Auerbacher Stra&#223;e</option>
    <option value="83">Auerstra&#223;e</option>
    <option value="84">Augsburger Stra&#223;e</option>
    <option value="85">Augsburger Stra&#223;e</option>
    <option value="86">Augustastra&#223;e</option>
    <option value="87">Auguste-Viktoria-Stra&#223;e</option>
    <option value="88">Avus</option>
    <option value="89">Avus</option>
    <option value="90">Axel-Springer-Stra&#223;e</option>
    <option value="91">Babelsberger Stra&#223;e</option>
    <option value="92">Badenallee</option>
    <option value="93">Badensche Stra&#223;e</option>
    <option value="94">Baerwaldstra&#223;e</option>
    <option value="95">Bahnhofstra&#223;e</option>
    <option value="96">Bahrfeldtstra&#223;e</option>
    <option value="97">Ballenstedter Stra&#223;e</option>
    <option value="98">Bamberger Stra&#223;e</option>
    <option value="99">B&#228;nschstra&#223;e</option>
    <option value="100">Barbarossastra&#223;e</option>
  </select>
</body>
</html>