<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>element_srolldown</title>
<script type="text/javascript">
  $(function() {
    
    var $select = $('#customSelect')
    var $counter = $('#counter')
    var displaySize = 5
    var optionEleHeight

    $counter.text(displaySize)

    $select.scroll(function(e) {
      
      optionEleHeight = optionEleHeight || this[0].offsetHeight
      currentBottomEleNo = this.size + Math.round(e.currentTarget.scrollTop / optionEleHeight)

      $counter.text(currentBottomEleNo)

    }).on('change blur', function() {
      this.size = 0
      $counter.hide()
    }).on('mousedown', function() {
      if (this.options.length > displaySize) {
        this.size = displaySize
        $counter.fadeIn()
      }
    })

  })
</script>
<style type="text/css">
.selectContainer {
  position: relative;
  width: 200px;
}

#customSelect {
  width: 200px;
}

#counter {
  position: relative;
  right: 20px;
  top: -40px;
  float: right;
  color: #1CD1D8;
  background-color: #444;
  border-radius: 20px;
  padding: 8px;
  min-width: 20px;
  text-align: center;
  display: none;
}
</style>
</head>
<body>
  <div class=selectContainer>
    <select id="customSelect">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
      <option>6</option>
      <option>7</option>
      <option>8</option>
      <option>9</option>
      <option>10</option>
      <option>11</option>
      <option>12</option>
      <option>13</option>
      <option>14</option>
      <option>15</option>
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
      <option>6</option>
      <option>7</option>
      <option>8</option>
      <option>9</option>
      <option>10</option>
      <option>11</option>
      <option>12</option>
      <option>13</option>
      <option>14</option>
      <option>30</option>
    </select>
    <div id=counter></div>
  </div>
</body>
</html>