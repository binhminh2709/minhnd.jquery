<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Use images like checkboxes</title>
  <style type="text/css">
    ul {
    list-style-type: none;
  }
  
  li {
    display: inline-block;
  }
  
  input[type="checkbox"][id^="cb"] {
    display: none;
  }
  
  label {
    border: 1px solid #fff;
    padding: 10px;
    display: block;
    position: relative;
    margin: 10px;
    cursor: pointer;
  }
  
  label:before {
    background-color: white;
    color: white;
    content: " ";
    display: block;
    border-radius: 50%;
    border: 1px solid grey;
    position: absolute;
    top: -5px;
    left: -5px;
    width: 25px;
    height: 25px;
    text-align: center;
    line-height: 28px;
    transition-duration: 0.4s;
    transform: scale(0);
  }
  
  label img {
    height: 100px;
    width: 100px;
    transition-duration: 0.2s;
    transform-origin: 50% 50%;
  }
  
  :checked + label {
    border-color: #ddd;
  }
  
  :checked + label:before {
    content: "✓";
    background-color: grey;
    transform: scale(1);
  }
  
  :checked + label img {
    transform: scale(0.9);
    box-shadow: 0 0 5px #333;
    z-index: -1;
  }
  </style>
</head>
<body>
<ul>
  <li>
    <input type="checkbox" id="cb1" />
    <label for="cb1"><img src="http://lorempixel.com/100/100" /></label>
  </li>
  <li>
    <input type="checkbox" id="cb2" />
    <label for="cb2"><img src="http://lorempixel.com/101/101" /></label>
  </li>
  <li>
    <input type="checkbox" id="cb3" />
    <label for="cb3"><img src="http://lorempixel.com/102/102" /></label>
  </li>
  <li>
    <input type="checkbox" id="cb4" />
    <label for="cb4"><img src="http://lorempixel.com/103/103" /></label>
  </li>
</ul>

<!-- 
http://stackoverflow.com/questions/30663562/use-images-like-checkboxes?utm_medium=social&utm_source=facebook.com&utm_campaign=so-facebook
 -->

</body>
</html>
