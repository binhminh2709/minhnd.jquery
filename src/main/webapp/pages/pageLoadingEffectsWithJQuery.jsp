<!DOCTYPE >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PageLoading</title>

  <script type="text/javascript" src="../js/jquery/jquery.js" /></script>

<script type="text/javascript">
		$(document).ready(function(){	
			$('#page_effect').fadeIn(2000);
		});
	</script>
</head>
<body>

	<div id="page_effect" style="display: none;">
		<!-- this content is hide, need jQuery to fade in -->
		<h1>JQuery Page Loading Effect</h1>

		This is page loading effect with JQuery 1<BR> This is page loading effect with JQuery 2<BR> This is page
		loading effect with JQuery 3<BR> This is page loading effect with JQuery 4<BR> This is page loading effect
		with JQuery 5<BR> This is page loading effect with JQuery 6<BR> This is page loading effect with JQuery 7<BR>
		This is page loading effect with JQuery 8<BR> This is page loading effect with JQuery 9<BR> This is page
		loading effect with JQuery 10<BR> This is page loading effect with JQuery 11<BR> This is page loading effect
		with JQuery 12<BR> This is page loading effect with JQuery 13<BR> This is page loading effect with JQuery 14<BR>
	</div>

	<button id="PageRefresh">Refresh a Page in jQuery</button>

	<script type="text/javascript">
		$('#PageRefresh').click(function() {
			location.reload();
		});
	</script>

	<h1>Highlight table row record on hover - jQuery</h1>

	<table border="1">
		<tr>
			<th>No</th>
			<th>Name</th>
			<th>Age</th>
			<th>Salary</th>
		</tr>
		<tr>
			<td>1</td>
			<td>Yong Mook Kim</td>
			<td>28</td>
			<td>$100,000</td>
		</tr>
		<tr>
			<td>2</td>
			<td>Low Yin Fong</td>
			<td>29</td>
			<td>$90,000</td>
		</tr>
		<tr>
			<td>3</td>
			<td>Ah Pig</td>
			<td>18</td>
			<td>$50,000</td>
		</tr>
		<tr>
			<td>4</td>
			<td>Ah Dog</td>
			<td>28</td>
			<td>$40,000</td>
		</tr>
		<tr>
			<td>5</td>
			<td>Ah Cat</td>
			<td>28</td>
			<td>$30,000</td>
		</tr>
	</table>

	<script type="text/javascript">
		$("tr").not(':first').hover(function() {
			$(this).css("background", "yellow");
		}, function() {
			$(this).css("background", "");
		});
	</script>
</body>
</html>
