<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ajax Progress</title>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="description" content="">
<meta name="viewport" content="width=device-width">

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

<!-- Google CDN -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="../js/jquery-ajax-progress/js/jquery.ajax-progress.js"></script>
</head>
<script>
  $(function() {
    $('#prog').progressbar({
      value : 0
    });
    
    $.ajax({
      method : 'GET',
      url: '../js/jquery-ajax-progress/data/bird.json',
      //url: 'http://api.openweathermap.org/data/2.5/weather?id=5128581&units=imperial',
      dataType : 'json',
      success : function(data) {
        //console.log('success YAYE!', arguments[0]);
        console.log('success YAYE!', data);
        console.log(data.animation);
        console.log(data.bones);
        console.log(data.colors);
        console.log(data.faces);
        console.log(data.materials);
        console.log(data.metadata);
        console.log(data.morphTargets);
        console.log(data.normals);
        console.log(data.scale);
        console.log(data.skinIndices);
        console.log(data.skinWeights);
        console.log(data.uvs);
        console.log(data.vertices);
      },
      error : function() {
        console.log('error AWWW!');
      },
      progress : function(e) {
        console.log('progress hey!', e);
        if (e.lengthComputable) {
          var pct = (e.loaded / e.total) * 100;
          $('#prog').progressbar('option', 'value', pct).children('.ui-progressbar-value').html(pct.toPrecision(3) + '%').css('display', 'block');
        } else {
          console.warn('Content Length not reported!');
        }
      }
    });
  });
</script>
</head>
<body>
  <div id="prog"></div>
</body>
</html>